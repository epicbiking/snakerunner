
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 * This class listens to the button presses of the ButtonPanel and 
 * performs the necessary operations of those button presses.
 * 
 * @author Myron Burton
 *
 */
public class ButtonListner implements ActionListener {

	// creates access to the snakeRunner application
	public SnakeRunner snakeRunner;
	
	/**
	 * This function will detect the input from the button panel 
	 * and will move the snake accordingly.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		// copied code from the calculatorListner class.
        Object sourceOfEvent = e.getSource();           // Get the source object of the event.
        JButton sourceButton = (JButton) sourceOfEvent; // Change our view of the reference
        String buttonLabel = sourceButton.getText();    // Get the button label.
        // end copied code
        
        // switch to call the specified direction from the button press
        switch (buttonLabel) {
        case "up":
        	snakeRunner.doMoveUp();
        	break;
        case "down":
        	snakeRunner.doMoveDown();
        	break;
        case "left":
        	snakeRunner.doMoveLeft();
        	break;
        case "right":
        	snakeRunner.doMoveRight();
        	break;
        }
		
	}

}
