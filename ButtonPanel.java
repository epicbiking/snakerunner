
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * This class builds a button panel and displays it next to the SnakeRunner application.
 * 
 * @author Myron Burton
 *
 */
public class ButtonPanel implements Runnable {

	// a way of accessing the snakeRunner application
	public SnakeRunner snakeRunner;
	
	public static void main(String[] args) {

		// create button panel
		ButtonPanel buttons = new ButtonPanel();
		
		// run button panel
		SwingUtilities.invokeLater(buttons);

	}
	
	@Override
	public void run () {
		
		// create up, down, left, and right buttons with appropriate titles
		JButton up = new JButton("up");
        JButton down = new JButton("down");
        JButton left = new JButton("left");
        JButton right = new JButton("right");
        
        // create a new window and panel to place buttons
        JFrame buttonWindow = new JFrame ("Controls");
        JPanel buttonPanel = new JPanel();
                        
        // create filler panels to create proper spacing when building the
        // button panel.
        JPanel filler0 = new JPanel();
        JPanel filler1 = new JPanel();
        JPanel filler2 = new JPanel();
        JPanel filler3 = new JPanel();
        JPanel filler4 = new JPanel();

        // remove the menu bar from the window
        buttonWindow.setUndecorated(true);
        
        // set the size of the button panel
        buttonPanel.setSize(200, 200);
        
        // set rules of the window. No resizing of the panel.
		buttonWindow.setResizable (false);
		buttonWindow.setSize(buttonPanel.getHeight(), buttonPanel.getWidth());
		buttonWindow.setVisible(true);
		
		// create a grid layout of 3 by 3 for the up down left and right buttons
		GridLayout buttonLayout = new GridLayout(3,3);
		// declare layout for the panel
        buttonPanel.setLayout(buttonLayout);
        
        // add the buttons and the filler panels in designated order
        // up on top left on the left, right on the right, and down on the bottom
        buttonPanel.add(filler0);
        buttonPanel.add(up);
        buttonPanel.add(filler1);
        buttonPanel.add(left);
        buttonPanel.add(filler2);
        buttonPanel.add(right);
        buttonPanel.add(filler3);
        buttonPanel.add(down);
        buttonPanel.add(filler4);

        // add panel to the window
		buttonWindow.add(buttonPanel, BorderLayout.NORTH);
        buttonWindow.setLayout(null);
		
        // create ButtonListner object
		ButtonListner buttons = new ButtonListner();
		
		// use buttons to control the snakeRunner applcation
		buttons.snakeRunner = snakeRunner;
        up.addActionListener (buttons);
        down.addActionListener (buttons);
        left.addActionListener (buttons);
        right.addActionListener (buttons);
		
        // This timer is used to locate the button window with the snake runner application.
        // This window will always appear next to the game. 
		int delay = 1; 
		ActionListener taskPerformer = new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// create a new point on the screen. The point is originally based on the 0, 0 coordinates
				// of the SnakeRunner application. Then it is modified by -200 in the x direction.
				Point p = new Point(SnakeRunner.location());
				p.setLocation(p.getX() - 200, p.getY());
				buttonWindow.setLocation(p);
			}
		};
		// start timer
		new Timer(delay, taskPerformer).start();
		
	}

}
