
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;


/**
 * This class builds cherries on screen at a random location.
 * 
 * @author Myron Burton
 *
 */
@SuppressWarnings("serial")
public class Cherry extends Canvas {
		
		
	@Override
	public void paint(Graphics g) {
		
		// set color of cherry to red
		g.setColor(Color.RED);
		
		// draw cherry as a circle inside of the canvas
		g.fillOval(0, 0, 15, 15);
	}
	/**
	 * Creates a new SnakeBodyPart object at a certain location.
	 * 
	 * @param x any x location on the screen
	 * @param y any y location on the screen
	 */
	public Cherry () {
			 
		// As a note, this class does not actually draw rectangles. It just creates blank canvases
		// which are filled grey. This is intentional.
		
	    Color grass = new Color(40, 180, 70);

		// set the background color of the snake
		setBackground(grass);		
		
		
		
		// set the size to 15 points by 15 points
		setSize(15, 15);
		
		// generate a random x position in the size of the main panel
		int x = (int) ((Math.random()) * 635);
		
		// generate a random y position in the size of the main panel. The minus 20
		// makes sure that it doesn't appear off of the screen due to the menu bar.
		int y = (int) ((Math.random()) * (640 * 16 / 9 - 40));
		
		// modify the x and y positions to be the same positions as a possible 
		// snake body part location
		if ((x - 3) % 18 != 0)
			x = x - ((x - 3) % 18);
		if ((y - 3) % 18 != 0)
			y = y - ((y - 3) % 18);
			
		
		// set location of the canvas.
		setLocation(x, y);
	}
}

