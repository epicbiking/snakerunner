
import java.util.*;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * This class builds a snake using SnakeBodyPart objects and creates a number
 * of interactions that we can use with the snake.
 * 
 * @author Myron Burton
 */
public class Snake {
	
	// create list of SnakeBodyPart objects to permit addition of
	// SnakeBodyPart objects in the future.
	private ArrayList<SnakeBodyPart> bodyParts = new ArrayList<SnakeBodyPart>();
	
	// public variable to store the length of the snake.
	public static int length;
	
	// public variable storing the current direction of the snake.
	public direction currentDirection;
	
	// set a panel to draw the snake and store it locally to access it when necessary.
	JPanel panel;
	
	SnakeRunner snakeRunner;
	/**
	 * This builds a snake using SnakeBodyPart objects.
	 * 
	 * @param length Length of the snake
	 * @param panel Panel where the snake will be drawn
	 */
	public Snake (int length, JPanel panel, SnakeRunner runner) {
		snakeRunner = runner;
		// create panel to draw the snake based on the function call
		this.panel = panel;
		// create length of the snake based on the function call
		Snake.length = length;
				
		// for the size of the snake build snake body parts
		for (int i = 0; i < length; i++) {
			
			// new snake body part with each piece located 15 points 
			// for size of snake body part and 3 points for padding to the right.
			SnakeBodyPart part = new SnakeBodyPart(3 + i * (15 + 3), 3);
			
			// once the part is created, add it to the list.
			bodyParts.add(part);
			
			// once the part is created, add it to the panel.
			panel.add(part);
		}
	}
	
	/**
	 * This function will move the snake based on the current direction of the snake.
	 * To change the direction of the snake change the direction enum to the corresponding
	 * direction.
	 */
	public void move () {
		moveBody();
		switch (currentDirection) {
		case right:
			moveRight();
			break;
		case left:
			moveLeft();
			break;
		case up:
			moveUp();
			break;
		case down:
			moveDown();
			break;
		}
		// if a collision is detected, stop moving the snake.
		if (didCollide() == true) {
	 		snakeRunner.stopSnakes();
			JOptionPane.showMessageDialog(null, "Final Score: " + length,"Score", 0);
			snakeRunner.startANewSnake();
		}
	}
	
	/**
	 * Manual call to move the snake up on block.
	 * This method also checks for collision and produces a score if it collides
	 */
	public void moveUp () {
		moveHead(0, -18);
	}

	/**
	 * Manual call to move the snake left on block.
	 * This method also checks for collision and produces a score if it collides
	 */
	public void moveLeft () {
		moveHead(-18, 0);
	}
	
	/**
	 * Manual call to move the snake down on block.
	 * This method also checks for collision and produces a score if it collides
	 */
	public void moveDown () {
		moveHead(0, 18);
	}
	
	/**
	 * Manual call to move the snake right on block.
	 * This method also checks for collision and produces a score if it collides
	 */
	public void moveRight () {
		moveHead(18, 0);
	}
	
	/**
	 * This function moves all parts of the snake, excluding the head,
	 * to the location of the preceding part of the snake.
	 */
	private void moveBody() {	
		for (int i = length - 1; i > 0; i--) {
			SnakeBodyPart part0 = bodyParts.get(i);
			SnakeBodyPart part1 = bodyParts.get(i - 1);
			part0.setLocation(part1.getLocation());
		}

	}
	
	/**
	 * This function moves only the head of the snake to whatever
	 * location is produced by a change in x and y coordinates passed
	 * in through the method call. For a snake with body parts size 15
	 * points square, the change would be +-18 where 3 points are padding.
	 * @param dx	change in x position of the head
	 * @param dy change in y position of the head
	 */
	private void moveHead(int dx, int dy) {
		SnakeBodyPart head = bodyParts.get(0);
		head.setLocation(head.getX() + dx, head.getY() + dy);				
	}
	
	/**
	 * Checks to see if the snake tries to make an illegal move.
	 * Also adds a body part in the event of the snake eating a cherry
	 * as running into a cherry is a type of collision
	 * @return boolean
	 */
	private boolean didCollide() {
		// if it collides with a cherry the snake grows and the 
		// cherry disappears.
		if (withCherry()) {
			addBodyPart();
			return false;
		}
		
		// if it collides with a wall or another body part then it
		// will return true indicating collision
		else if (withWall()) {
			return true;
		}
		else if (withBody()) {
			return true;
		}
		
		//otherwise, return false
		return false;
	}
	
	private boolean withCherry () {
		// find the head of the snake
		SnakeBodyPart head = bodyParts.get(0);
		
		// check the location of the head of the snake with the location of the cherry
		if (snakeRunner.cherry.getX() == head.getX() && snakeRunner.cherry.getY() == head.getY()) {
			// remove the old cherry
			panel.remove(snakeRunner.cherry);
			// create a new cherry
			snakeRunner.cherry = new Cherry();
			// add a body part
			addBodyPart();
			// show new cherry on the screen
			panel.add(snakeRunner.cherry);
		}
		return false;
	}

	/**
	 * checks to see if the snake has run into a wall
	 * @return boolean
	 */
	private boolean withWall () {
		// get the head of the snake
		SnakeBodyPart head = bodyParts.get(0);
		
		// check to see if it is leaving the bounds of the game
		if (head.getX() < 0 || head.getX() > 640 - 20)
			// the minus 20 is determined by a block size 15 plus wall padding of 5
			return true;
		if (head.getY() < 0 || head.getY() > (640 * 16 / 9 - 40))
			// the minus 40 is determined by a block size 15 plus wall padding of 5 plus 20 for the menu bar.
			return true;
		return false;
	}

	/**
	 * checks to see if the snake has run into itself
	 * @return boolean
	 */
	private boolean withBody () {
		// get the head of the snake
		SnakeBodyPart head = bodyParts.get(0);
		
		// check all other body parts for identical x and y coordinates
		for (int i = 1; i < length; i++) {
			SnakeBodyPart bodyPart = bodyParts.get(i);
			if (head.getX() == bodyPart.getX() && head.getY() == bodyPart.getY())
				return true;
		}
		return false;
	}

	
	/**
	 * This function adds one body part to the snake.
	 */
	public void addBodyPart () {
		
		// store the object of the last body part in the array
		SnakeBodyPart lastBodyPart = bodyParts.get(length - 1);
		// create a new body part using the location of the last body part
		SnakeBodyPart newBodyPart = new SnakeBodyPart(lastBodyPart.getX(), lastBodyPart.getY());
		
		// add the new body part to the visible area
		panel.add(newBodyPart);
		// add the new body part to the list of body parts
		bodyParts.add(newBodyPart);
		
		// here we move all of the body parts excluding the last one because the length
		// of the snake hasn't been updated. This way the snake moves revealing the new
		// body part hidden under the last body part. Then the length is updated so that
		// all body parts move normally.
		move();
		length++;
		
	}
	
	public void removeSnake () {
		while (length > 0) {
			length--;
			SnakeBodyPart bodyPart = bodyParts.get(length);
			panel.remove(bodyPart);
			bodyParts.remove(length);
		}
	}
}
