
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

/**
 * This class creates a snake body part object which appears as a 15 point square
 * of the color grey.
 * 
 * @author Myron Burton
 *
 */
@SuppressWarnings("serial")
public class SnakeBodyPart extends Canvas {
	
	
	 @Override
	public void paint(Graphics g) {

	}
	 /**
	  * Creates a new SnakeBodyPart object at a certain location.
	  * 
	  * @param x any x location on the screen
	  * @param y any y location on the screen
	  */
	 public SnakeBodyPart (int x, int y) {
		 
		 // As a note, this class does not actually draw rectangles. It just creates blank canvases
		 // which are filled grey. This is intentional.
		 
		 // set the background color of the snake
		 setBackground(Color.GRAY);
		 
		 // set the size to 15 points by 15 points
		 setSize(15, 15);
		 
		 // set location of the canvas.
		 setLocation(x,y);
	 }
}
