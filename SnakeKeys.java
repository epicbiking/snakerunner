
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
public class SnakeKeys implements KeyListener {

	// create access to SNakeRunner application
	public SnakeRunner snakeRunner;
	public ButtonPanel buttonPanel;
	
	// constructor for snake keys
	public SnakeKeys(SnakeRunner snakeRunner) {
		this.snakeRunner = snakeRunner;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println(e.getExtendedKeyCode());
		// switch statement to control where the snake goes
		// using WASD or the arrow keys.
		switch(e.getExtendedKeyCode()) {
		// key right
		case 39:
			snakeRunner.doMoveRight();
			break;
			
		// key up
		case 38:
			snakeRunner.doMoveUp();
			break;
			
		// key down
		case 40:
			snakeRunner.doMoveDown();
			break;
			
		// key left
		case 37:
			snakeRunner.doMoveLeft();
			break;
		
		// key 'd'
		case 68:
			snakeRunner.doMoveRight();
			break;
				
		// key 'w'
		case 87:
			snakeRunner.doMoveUp();
			break;
				
		// key 's'
		case 83:
			snakeRunner.doMoveDown();
			break;
				
		// key 'a'
		case 65:
			snakeRunner.doMoveLeft();
			break;
			
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
