
/**
 * This is the main application file for Snake Runner. It builds the application window
 * and accesses the other SnakeRunner classes.
 * 
 * @author Myron Burton
 */

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class SnakeRunner implements Runnable {
	
	// declare JPanel where game will be drawn and a public snake object
	static JPanel mainPanel;
	
	// create public snake to use anywhere
	public Snake snake;
	
	// create public cherry object
	public Cherry cherry = new Cherry();
	
	public SnakeKeys keyListener = new SnakeKeys(this);
	
	// declare public timer to be able to stop it from other locations
	public Timer timer;

	
	public static void main(String[] args) {
		
		// set up the applications for the game and for the button panel.
		SnakeRunner app = new SnakeRunner();
		ButtonPanel buttons = new ButtonPanel();
		buttons.snakeRunner = app;
		
		// launch both games
		SwingUtilities.invokeLater(buttons);
		SwingUtilities.invokeLater(app);
		
	}
	
	public static Point location() {
		return mainPanel.getLocationOnScreen();
	}

	@Override
	public void run() {

		// set up new JFrame where the panel will be displayed.
        JFrame window = new JFrame ("Snake Runner");
        mainPanel = new JPanel();
        
        // create key listener and add it to the main window.
        window.addKeyListener(keyListener);
        
       // create a color for later use. 
        Color grass = new Color(40, 180, 70);
        
        // close application when a window is closed.
        window.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        
       // set standard width and accompanying height is automatically generated
        int size = 640;
		window.setSize (size, size * 16/9);
		
		// window attributes
        window.setResizable (false);
        window.setLocationRelativeTo(null); // Centers the window on the screen
        window.setVisible (true);
        
        // include mainPanel in the window and set backGround color of the panel
        window.add(mainPanel);
        mainPanel.setBackground(grass);
        
        // disable layout managers.
        mainPanel.setLayout(null);

    	// create a clock and make it update the position every 100 ms.
		int delay = 100;
		ActionListener taskPerformer = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
					snake.move();
			}
		};
		// start the timer.
		timer = new Timer(delay, taskPerformer);

		startANewSnake();
		// place cherry on screen
		mainPanel.add(cherry);

	}
	public void stopSnakes() {
		timer.stop();
	}

	public void startANewSnake() {
		if (snake != null) {
			snake.removeSnake();
		}

		snake = new Snake(3, mainPanel, this);
		// set a current direction for the snake to start off with.
		snake.currentDirection = direction.down;
		timer.start();
	}

	// four functions to do on the snake object. for each case 
	// we call the function inside of the snake class.
	
	/**
	 * This class makes the snake change directions and start moving up as long
	 * as the current direction is not the opposite direction called. This way
	 * the snake does not run on top of itself.
	 */
	public void doMoveUp() {
		// make sure that the current direction is not down and
		// update the direction of the snake.
		if (snake.currentDirection != direction.down)
			snake.currentDirection = direction.up;
	}
	
	/**
	 * This class makes the snake change directions and start moving down as long
	 * as the current direction is not the opposite direction called. This way
	 * the snake does not run on top of itself.
	 */
	public void doMoveDown() {
		// make sure that the current direction is not up and
		// update the direction of the snake.
		if (snake.currentDirection != direction.up)
			snake.currentDirection = direction.down;
	}

	/**
	 * This class makes the snake change directions and start moving left as long
	 * as the current direction is not the opposite direction called. This way
	 * the snake does not run on top of itself.
	 */
	public void doMoveLeft() {
		// make sure that the current direction is not right and
		// update the direction of the snake.
		if (snake.currentDirection != direction.right)
			snake.currentDirection = direction.left;		
	}

	/**
	 * This class makes the snake change directions and start moving right as long
	 * as the current direction is not the opposite direction called. This way
	 * the snake does not run on top of itself.
	 */
	public void doMoveRight() {
		// make sure that the current direction is not left and
		// update the direction of the snake.
		if (snake.currentDirection != direction.left)
			snake.currentDirection = direction.right;
	}
	

}
